var config = process.env.ENV_TYPE === 'production' ? require('./webpack.base.config.js') : require('./webpack.dev.config.js');

module.exports = config;
