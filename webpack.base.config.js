// Global hint: "Keep in mind that you don’t need to write pure JSON into the configuration. Use any JavaScript you want. It’s just a node.js module…"
var path               = require('path'),
    srcPath            = path.join(__dirname, 'src'),
    distPath           = path.join(__dirname, 'dist'),
    CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin"),
    NoErrorsPlugin     = require("webpack/lib/NoErrorsPlugin"),
    DefinePlugin       = require("webpack/lib/DefinePlugin"),
    HtmlWebpackPlugin  = require('html-webpack-plugin'),
    ENV_TYPE           = process.env.ENV_TYPE;

module.exports = {
  //Sample: Entry as a single chunk
  // entry: ["./global.js", "./app.js"], //get those 2 files and output them as bundle.js
  //Entry as multi chunks
  entry: {
    //separated into chunks: bundle and common
    bundle: path.join(srcPath, 'main.js'),
    common: [path.join(srcPath, 'global'), 'react', 'react-router'] //common libs: example of global.js and react vendor
  },
  output: {
    path: distPath, // output entry to ./dist
    filename: "[name].js", //[name] is a chunk name from entry object properties
    pathInfo: true,
    publicPath: ''
  },
  module: {
    preLoaders: [
      {test: /\.js$/, loader: 'eslint-loader', exclude: /node_modules/} //config at .eslintrc
    ],
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
        // babel-loader:
        // all ES6 and JSX scripts will be transpiled to old-style JS (ES5)
      },
      {
        test: /\.scss$/,
        loader: 'style!css!sass'
        // sass loader, more info: https://github.com/jtangelder/sass-loader
      }
    ]
  },
  plugins: [
    new CommonsChunkPlugin('common', 'common.js'),
    // CommonsChunkPlugin will create react lib (and others from common chunk) as common.js, w/o this plugin react lib will be in bundle.js and common.js casue main.js require react
    new NoErrorsPlugin(),
    // NoErrorsPlugin doesn't create output if there is any error, more: http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
    new HtmlWebpackPlugin({
      // inject: true,
      template: 'src/index.html'
    }),
    // HtmlWebpackPlugin serve index.html template to output (./dist), more: https://www.npmjs.com/package/html-webpack-plugin
    new DefinePlugin({
      DEV: ENV_TYPE === 'development' || typeof ENV_TYPE === 'undefined',
      PRODUCTION: ENV_TYPE === 'production'
    })
    // DefinePlugin creates constants, more: http://webpack.github.io/docs/list-of-plugins.html#defineplugin
  ],
  resolve: {
    extensions: ['', '.js', '.scss'],
    modulesDirectories: ['node_modules', 'src']
    // More: http://webpack.github.io/docs/configuration.html#resolve-modulesdirectories
  },
  //devServer option is used to configure WebpackDevServer
  devServer: {
    contentBase: './dist', //content will be served from ./dist
  }
  //Additionals config options
  // cache: boolean,
    // More: http://webpack.github.io/docs/configuration.html#cache
  // debug: boolean,
    // More: http://webpack.github.io/docs/configuration.html#debug
  // devtool: string,
    // More: http://webpack.github.io/docs/configuration.html#devtool
  // watch: boolean,
    // this is used as --watch flag at package.json as npm run watch command
    // “The dev server uses Webpack’s watch mode. It also prevents webpack from emitting the resulting files to disk. Instead it keeps and serves the resulting files from memory.” — This means that you will not see the webpack-dev-server build in your_file.js, to see and run the build, you must still run the webpack command.
  //More webpack conf options http://webpack.github.io/docs/configuration.html
}
