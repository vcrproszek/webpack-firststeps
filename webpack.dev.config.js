'use strict';

var config = require('./webpack.base.config.js');

if (config.entry.bundle) {
  config.entry.bundle = [
    'webpack/hot/dev-server',
    config.entry.bundle
  ];
};

// config.devtool = 'eval-source-map';
config.devtool = 'source-map';
// More: https://webpack.github.io/docs/configuration.html#devtool
config.module.loaders = [
  {
    test: /\.js?$/,
    loaders: ['react-hot', 'babel-loader'],
    exclude: /node_modules/
    // react-hot, more: https://github.com/webpack/docs/wiki/hot-module-replacement-with-webpack, https://gaearon.github.io/react-hot-loader/
    // It’s worth noting that not all modules can be replaced. The code in src/main.js cannot be reloaded and will cause a full page reload but changing the hello.js component will trigger a hot module replacement.
  },
  {
    test: /\.scss$/,
    loader: 'style!css!sass'
    // sass loader, more info: https://github.com/jtangelder/sass-loader
  }
];

module.exports = config;
