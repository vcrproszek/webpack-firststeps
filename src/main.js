// Bootstrapping module
import 'styles/main.scss';

import React from 'react';
import {default as Router, Route} from 'react-router';
// import Router from 'react-router';

import App from 'components/app'
import SubPage from 'components/subpage'

const routes = (
  <Route name="homepage" path="/" handler={App}>
    <Route name="subpage" path="/subpage" handler={SubPage}/>
  </Route>
);

Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.body);
});
