import React from 'react';
import {Link} from 'react-router';

class Menu extends React.Component {
  render() {
    return (
      <header>
        <h2>Yet another Webpack Boilerplate</h2>
        <ul>
          <li><Link to='homepage'>Homepage</Link></li>
          <li><Link to='subpage'>Subpage</Link></li>
        </ul>
      </header>
    );
  }
}

export default Menu;
