import React from 'react';
import {Link} from 'react-router';

export default class SubPage extends React.Component {
  render() {
    return (
      <div>
        <h2>Here is simple subpage</h2>
        <Link to='homepage'>back to homepage</Link>
      </div>
    );
  }
}
