import React from 'react';
import {RouteHandler} from 'react-router';
//More about import/export in ES6
// http://www.2ality.com/2014/09/es6-modules-final.html
// https://muffinresearch.co.uk/back-to-the-future-using-es6-with-react/
// https://github.com/rackt/react-router/issues/1370#issuecomment-113964759
import Menu from 'components/menu'

//old way
// var App = React.createClass({
//   render: function () {
//     return (
//       <div className="wrapper">
//         <Menu/>
//         <div id="main">
//           <RouteHandler/>
//         </div>
//       </div>
//     );
//   }
// });

export default class App extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <Menu/>
        <div id="main">
          <RouteHandler/>
        </div>
      </div>
    );
  }
}
