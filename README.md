# webpack-firststeps
This is simple app written in JS ES6 used with webpack as module bundler.

## Requirements
Make sure you have installed NodeJS and NPM first and that you can run them from the command line.
* Run `npm install` first to install dependencies

### To develop install:
* `npm install webpack -g`
* `npm install webpack-dev-server -g`

## Commands
* `npm run start` - Start the Webpack dev server
* `npm run dev` - Start the Webpack dev server (with watch and more debug)
* `npm run dist` - Build to production (Set fisrt: `$ env ENV_TYPE='production'`)
